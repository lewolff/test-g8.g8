
lazy val root = (project in file("."))
  .enablePlugins(ScriptedPlugin)
  .settings(
    name := "test-g8.g8",
    scalaVersion := "3.3.1",
    scalacOptions ++= Seq("-deprecation"),
  )

